# oh wow 3d engine cool cool

yeah just for fun CPU based real-time rendering engine

it's built as an instructive project so I'm not using openGL or anything which
means everything that happens from loading a mesh to displaying every pixel
on screen happens inside this code, which i think is cool but that means it
doesn't take advantage of the GPU and does everything very slowly on the CPU.

## features

can display stuff in 3d !

## instructions for the included demo (main.rs)

- pan the camera with right click
- up/down arrows to move
- `W` to toggle wireframe mode
- yes i know the controls are aweful
- press 1 & 2 to change scenes (this is badly done for now)
- `E` or `Escape` to exit

please compile & run with `cargo run --release` or else it'll be super slow

`cargo doc --open` for more info (and example code), even though the doc is
incomplete

## known bugs
- running in debug mode will sometimes make the program panic. This is because
we don't eliminate triangles that are outside our view before projecting. This
means we sometimes project things that are going to create extreme values (
for instance a point to our left, with a value of z close to zero).
They are then eliminated but not before it could trigger an overflow. For some
reason release mode seems to allow overflow so we're good (still need to fix
that)
- since there is no near plane clipping, geometry will disappear if it
intersects with the z plane. Not really a bug but looks like one.
- setup causing a panic in the renderer:

```rust
let mut plane = lib3d::primitives::plane(39);
plane.scale((40.0, 40.0, 40.0));
plane.set_position(Vec3::new(0.0, -5.0, 0.0));
scene.add_mesh(RefCell::new(plane));
```

with the camera being :

```rust
let mut camera = Transformation{
	translation: Vec3::new(0.0, -2.0, 5.0),
	rotation: Mat3::identity(),
};
```

note that this only seems to work for n = 39 (which corresponds to 40 subdivisions).
This is probably caused by the plane grid lining up with z = 0

## interrogations
should we really use that structure of vertex array and face that have an index
to that array ? Did it that way cause OBJ format but it's optimised for size
not speed. It might be faster to have the actual coordinates in the face
struct. We duplicate a lot of vertices that way though, need to test that


## roadmap / todo (not ordered)

### look into drawing using scan lines
render pixel by pixel and find the value of that pixel by having all the
objects of the scene in the renderer. This should be more efficient than
what we're doing now because we have to read/write a lot from two large
buffers which is definitely not cache-friendly. I suspect it's the main
source of non-speed in the engine right now.

### z-buffer
we use 2d interpolation for now, look into getting into the third dimension
(intersect camera ray with triangle, get barycentric coordinates of
intersection point)

### near-plane clipping
so that you can be close to meshes with large triangles without the whole thing
disappearing

### optimisation
- look into maybe using SIMD ? idk sounds interesting (barycentric calc)
- multi threading at the triangle level should be looked at.
or maybe even multiple computing threads + 1 drawing thread ???
- even very simple objects, when taking most of the screen, will cause a huge
slow down of this thing. Hence the following point:
- compare different triangle drawing methods to maybe find a better one
- optimise for cache ? look into that 

- for later : maybe order objects by their z position so that you draw the
front objects first. This way you do less actual drawing.
- ultimate optimisation: scratch the entire project and use opengl

### texturing
- figure out how to do it but i have a feeling that the barycentric deal will help
us + probably you need 3d not to have ugly perspective deformation
- load texture from .objs

### consider rendering to HDR buffer instead of 24 bit color
i don't know how costly this would be, but it could be cool to have a high
dynamic range image to work with, especially for eventual post-processing.
Would also make a cleaner pipeline I think.

This could also be optional ! So that if you don't need it you don't get
the overhead

### smooth shading + phong shading
it's just cool

### light
- do a bit of research to see how off my implementation of lighting is
(should the mapping from "true" color to 24bit color be linear as it is ?
or use a fancier function (log ?))
- the renderer should have an exposure setting that will change how we process
the lighting

### load more stuff from OBJs
- vertex normals
- vertex texture coordinates

### moving away from mini\_fb
sorry but you don't work well with wayland + you're slow -> try to use pixels + winit

### misc
- remove #[!allow\_dead\_code] at some point
- my draw triangle function is stolen from somewhere => make sure that it's
efficient
- utilities : display text (framerate)
- have fun with a lib4d that projects 4d objects to a 3d scene (not using the
"slice" method but 4d perspective proj. so that objects that don't intersect
with w = 0 don't disappear but are smaller.
(analog to how 3d camera project on a 2d surface))

## resources
- https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/barycentric-coordinates
- https://codeplea.com/triangular-interpolation
- https://en.wikipedia.org/wiki/Blinn%E2%80%93Phong_reflection_model
- see if this is better by any chance : http://www.sunshine2k.de/coding/java/TriangleRasterization/TriangleRasterization.html
- this bloom tutorial is interesting: https://www.youtube.com/watch?v=tI70-HIc5ro
- seems super nice : https://fgiesen.wordpress.com/2013/02/17/optimizing-sw-occlusion-culling-index/
- interesting series about scan line rasterization: https://www.youtube.com/playlist?list=PLEETnX-uPtBUbVOok816vTl1K9vV1GgH5

