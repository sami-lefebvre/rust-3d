//! Convenient way (I hope) of handling multiple meshes and lights
//! 
//! See the main example in [lib3d](crate) to see how to set up a simple scene.


use crate::*;
use crate::renderer::{Renderer, Renderable};
use crate::lighting::{Lighting, Sun, AmbiantLighting};
use crate::trans::*;

/// Trait meant for 3d objects that are in the scene
pub trait Object3d: Renderable + InCamera + Transformable {}
// Blanked implementation so that if something has both renderable and incamera,
// it also is object3d automatically
impl<T: Renderable + InCamera + Transformable> Object3d for T {}

/// A Scene contains meshes, lights, and a camera. It manages the object transformations and rendering
/// with the [Scene::render] function.
/// 
/// Add the meshes you want to rendere via [Scene::add_mesh], and use the [Scene::lighting] struct to
/// tweak the lighting of the scene.
/// 
/// The camera is handled by [Scene::camera], which represents the transformation of the camera from
/// (0, 0, 0) to the desired position (camera looks in the +Z direction)
pub struct Scene {
	pub renderer: Renderer,
	pub objects_3d: Vec::<Box::<dyn Object3d>>, 
	pub lighting: Lighting,
	/// The transformation that represents the 3D camera.
	pub camera: Transformation,
	pub drawn_meshes: u32,
    pub drawn_triangles: u32,
}

impl Scene {
	pub fn new(renderer: Renderer) -> Scene {
		Scene {
			renderer,
			objects_3d: Vec::new(),
			lighting: Lighting::new(),
			camera: Transformation::new(Vec3::default(), Mat3::identity()),
			drawn_meshes: 0,
            drawn_triangles: 0,
		}
	}

	pub fn preset_scene(renderer: Renderer) -> Scene {
		let mut lighting = Lighting::new();
		lighting.suns.push(Sun::new(Vec3::new(1.0, 1.0, 0.5), colors::WHITE, 1.0));
		lighting.suns.push(Sun::new(Vec3::new(-1.0, 0.0, -0.5), colors::CYAN, 0.2));
		lighting.ambiant = AmbiantLighting::new(colors::WHITE, 0.1);

		Scene {
			renderer,
			objects_3d: Vec::new(),
			lighting,	
			camera: Transformation::new(Vec3::default(), Mat3::identity()),
			drawn_meshes: 0,
            drawn_triangles: 0,
		}
	}

	/*pub fn get_triangle_count(&self) -> u32{
		let mut triangle_count = 0_u32;
		for m in self.meshes.iter() {
			triangle_count += m.get_triangle_count();
		}
		triangle_count
	}*/

	pub fn get_drawn_triangle_count(&self) -> u32 {
		self.drawn_triangles
	}

	/// You shouldn't have to use this directly probably, but this applies the meshes' transformations
	/// as well as the camera transformations.
	pub fn transform(&mut self) {
		// self.camera = camera.clone();
		
		for m in self.objects_3d.iter_mut() {
			m.transform(&self.camera);
		}

		for s in self.lighting.suns.iter_mut() {
			s.transform(&self.camera);
		}

		for l in self.lighting.lamps.iter_mut() {
			l.transform(&self.camera);
		}
	}

	/// Adds a mesh to the scene to be rendered
	/// 
	/// Returns the index of the mesh in the array. Use that index with get_mesh_mut() to interact with the object
	pub fn add_object_3d(&mut self, object: Box<dyn Object3d>) -> usize {
		self.objects_3d.push(object);
		self.objects_3d.len() - 1
	}

	pub fn get_object_3d_mut(&mut self, index: usize) -> &mut Box<dyn Object3d> {
		&mut self.objects_3d[index]
	}

    pub fn object_3d_vec_mut(&mut self) -> &mut Vec<Box<dyn Object3d>> {
        &mut self.objects_3d
    }

	/// This function will apply the meshes' transformations as well as
	/// the inverse of the camera's transformations (to simulate the camera's position; we don't move
	/// the camera but everything else to trick you into thinking we do). It will also draw every mesh
	/// in the meshes array and return an image buffer.
	pub fn render(&mut self) -> &mut l2d::Buffer<u32>{
		self.transform();
		//let draw_time = 0;
		self.drawn_meshes = 0;
        self.drawn_triangles = 0;
		//optimisation here : sort by depth
		self.renderer.blank_with_color(l2d::Color::from_rgb(150, 150, 160));
		//self.renderer.times_debug = (0, 0, 0, 0);
		//let time = Instant::now();
        for m in self.objects_3d.iter() {
            let tris = m.render(&mut self.renderer, Some(&self.lighting));
            if tris.is_some() && tris.unwrap() > 0 {
                self.drawn_triangles += tris.unwrap();
		        self.drawn_meshes += 1;
            }
        }


		/*println!("mesured time: {}", time.elapsed().as_nanos() / 1000);
		println!("0 time: {:}", self.renderer.times_debug.0 / 1000);
		println!("1 time: {:}", self.renderer.times_debug.1 / 1000);
		println!("2 time: {:}", self.renderer.times_debug.2 / 1000);*/
		 

        self.renderer.get_image_mut()
	}
}
