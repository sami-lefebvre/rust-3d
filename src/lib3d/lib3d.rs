//! 3D rendering engine built to learn how this stuff works
//! 
//! Quite limited for now and not very fast, but it sure is fun
//! 
//! # Example
//! 
//! ```
//! use lib3d::*;
//! use lib3d::mesh::*;
//! use std::cell::RefCell;
//!
//! let renderer = Renderer::new(800, 600);
//! let mut scene = Scene::new(renderer);
//! 
//! let objs = lib3d::io::load_obj("assets/teapot.obj").unwrap();
//! let mut teapot = objs[0].to_owned();
//! let mut cube = lib3d::primitives::cube();
//! 
//! cube.set_position(Vec3::new(3.0, 0.0, 15.0));
//! cube.set_rotation(Mat3::rotation_y(1.0) * Mat3::rotation_x(1.0));
//! teapot.set_position(Vec3::new(-3.0, 0.0, 15.0));
//! teapot.shading = renderer::Shading::Smooth;
//! 
//! scene.add_mesh(teapot);
//! let cube_index = scene.add_mesh(cube);
//!
//! let sun = lighting::Sun::new(Vec3::new(-0.5, 1.0, -1.0), lib3d::colors::WHITE, 0.8);
//! scene.lighting.ambiant = lighting::AmbiantLighting::new(lib3d::colors::BLUE, 0.1);
//! scene.lighting.suns.push(sun);
//!
//! loop {
//!     scene.get_mesh_mut(cube_index).translate(Vec3::new(0.0, 0.01, 0.0)); // this is how you access meshes after adding the to the scene
//!     let image = scene.render().get_buffer();
//!     // use image to display the render
//!     break;
//! }
//! ```

//include!("utils.rs");
pub mod utils;
pub mod io;
pub mod mesh;
pub mod lighting;
pub mod scene;
pub mod trans;
pub mod renderer;
pub mod l2d;
pub mod magiceye;
pub mod material;
pub mod primitives;

pub use crate::utils::*;
pub use crate::scene::Scene;
pub use crate::renderer::Renderer;
pub use crate::mesh::Mesh;
pub use crate::trans::Transformable;

#[cfg(test)]
#[path = "../test/lib3d_test.rs"]
mod lib3d_test;