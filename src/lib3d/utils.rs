//! Mainly math stuff in there

use crate::l2d;

/// Represents a 3d vector
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Vec3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

/// Represents a 3x3 matrix
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Mat3 {
    pub a: (f32, f32, f32),
    pub b: (f32, f32, f32),
    pub c: (f32, f32, f32),
}

/// 3d space color
/// 
/// This is the true color of something. Used for meshes and lights.
/// 
/// To be rendered, it needs to be downgraded to a standard 24bit depth color ([crate::l2d::Color])
#[derive(Copy, Clone)]
pub struct Color {
    pub r: f32,
    pub g: f32,
    pub b: f32,

    //add brightness ?
}

impl Color {
    pub fn new(r: f32, g: f32, b: f32) -> Color{
        Color {
            r, g, b,
        }
    }
}

impl std::ops::Mul<f32> for Color {
    type Output = Color;

    fn mul(self, v: f32) -> Color {
        Color {
            r: self.r * v,
            g: self.g * v,
            b: self.b * v,
        }
    }
}

impl std::ops::Add<f32> for Color {
    type Output = Color;

    fn add(self, v: f32) -> Color {
        Color {
            r: self.r + v,
            g: self.g + v,
            b: self.b + v,
        }
    }
}

impl std::ops::Mul<Color> for Color {
    type Output = Color;

    fn mul(self, c: Color) -> Color {
        Color {
            r: self.r * c.r,
            g: self.g * c.g,
            b: self.b * c.b,
        }
    }
}

impl std::ops::Add<Color> for Color {
    type Output = Color;

    fn add(self, c: Color) -> Color {
        Color {
            r: self.r + c.r,
            g: self.g + c.g,
            b: self.b + c.b,
        }
    }
}

#[allow(dead_code)]
pub mod colors {
    use super::Color;
    pub static BLACK: Color = Color {r: 0.0, g: 0.0, b: 0.0};
    pub static WHITE: Color = Color {r: 1.0, g: 1.0, b: 1.0};
    pub static RED: Color = Color {r: 1.0, g: 0.0, b: 0.0};
    pub static GREEN: Color = Color {r: 0.0, g: 1.0, b: 0.0};
    pub static BLUE: Color = Color {r: 0.0, g: 0.0, b: 1.0};
    pub static MAGENTA: Color = Color {r: 1.0, g: 0.0, b: 1.0};
    pub static YELLOW: Color = Color {r: 1.0, g: 1.0, b: 0.0};
    pub static CYAN: Color = Color {r: 0.0, g: 1.0, b: 1.0};
}

impl Vec3 {
    pub fn new(x: f32, y: f32, z: f32) -> Vec3 {
        Vec3 {x, y, z}
    }
    pub fn project(&self, focal: f32, w: u32, h: u32) -> l2d::Pixel {
        let x = self.x / (focal * self.z);
        let y = (w as f32 / h as f32) * self.y / (focal * self.z);

        let mut x = (x * w as f32) as i32;
        let mut y = (y * h as f32) as i32;

        x += (w / 2) as i32; // panic here ? run in release mode (i know i know someday ill fix this)
        y = (h / 2) as i32 - y;

        l2d::Pixel::new(x, y)
    }
    
    pub fn get_magnitude(&self) -> f32 {
    	(self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
	}
	
	pub fn normalize(&mut self) -> &mut Vec3 {
		let mag = self.get_magnitude();
		self.x /= mag;
		self.y /= mag;
		self.z /= mag;
		
		self
	}
	
	/*pub fn clone(&self) -> Vec3 {
		Vec3::new(self.x, self.y, self.z)
	}*/
    
    /// Given three points, return the normal of the plan defined by those points
    pub fn find_normal(a: Vec3, b: Vec3, c: Vec3) -> Vec3 {
        let vec_b = Vec3::new(c.x - a.x, c.y - a.y, c.z - a.z);
        let vec_a = Vec3::new(b.x - a.x, b.y - a.y, b.z - a.z);

		let mut cp = Vec3::cross_product(vec_a, vec_b);
		cp.normalize();
		cp
    }
    

    pub fn cross_product(a: Vec3, b: Vec3) -> Vec3 {
    	Vec3::new(
			a.y * b.z - a.z * b.y,
			a.z * b.x - a.x * b.z,
			a.x * b.y - a.y * b.x,
		)
    }
    
    pub fn center_of_triangle(a: Vec3, b:Vec3, c: Vec3) -> Vec3 {
    	Vec3::new(
    		(a.x + b.x + c.x) / 3.0,
    		(a.y + b.y + c.y) / 3.0,
    		(a.z + b.z + c.z) / 3.0,
    	)
    }
    
    pub fn dot_product(a: Vec3, b: Vec3) -> f32 {
    	let mut product = 0 as f32;
    	
    	product += a.x * b.x;
    	product += a.y * b.y;
    	product += a.z * b.z;
    	
    	product
    }
    
    pub fn invert(&mut self) {
    	self.x = 0.0-self.x;
    	self.y = 0.0-self.y;
    	self.z = 0.0-self.z;
    }
    
    pub fn distance(a: &Vec3, b: &Vec3) -> f32 {
    	((b.x - a.x).powf(2.0) + (b.y - a.y).powf(2.0) + (a.z - b.z).powf(2.0)).sqrt()
    }
}

impl Default for Vec3 {
    fn default() -> Vec3 {
        Vec3 { x: 0.0, y: 0.0, z: 0.0 }
    }
}

impl std::ops::Sub<Vec3> for Vec3 {
    type Output = Vec3;

    fn sub(self, v: Vec3) -> Vec3 {
        Vec3::new(self.x - v.x, self.y - v.y, self.z - v.z)
    }
}

impl std::ops::Mul<f32> for Vec3 {
    type Output = Vec3;

    fn mul(self, v: f32) -> Vec3 {
        Vec3::new(self.x * v, self.y * v, self.z * v)
    }
}

impl std::ops::Div<f32> for Vec3 {
    type Output = Vec3;

    fn div(self, v: f32) -> Vec3 {
        Vec3::new(self.x / v, self.y / v, self.z / v)
    }
}

impl std::ops::Add<Vec3> for Vec3 {
    type Output = Vec3;

    fn add(self, v: Vec3) -> Vec3 {
        Vec3::new(self.x + v.x, self.y + v.y, self.z + v.z)
    }

}

impl Mat3 {
    pub fn new(a: (f32, f32, f32), b: (f32, f32, f32), c: (f32, f32, f32)) -> Mat3 {
        Mat3 {
            a,
            b,
            c,
        }
    }

    pub fn identity() -> Mat3 {
        Mat3::new(
            (1.0, 0.0, 0.0),
            (0.0, 1.0, 0.0),
            (0.0, 0.0, 1.0),
        )
    }

    pub fn rotation_x(a: f32) -> Mat3 {
        Mat3::new(
            (1.0, 0.0    , 0.0     ),
            (0.0, a.cos(), -a.sin()),
            (0.0, a.sin(), a.cos() ),
        )
    }

    pub fn rotation_y(a: f32) -> Mat3 {
        Mat3::new(
            (a.cos() , 0.0    , a.sin() ),
            (0.0     , 1.0    , 0.0     ),
            (-a.sin(), 0.0    , a.cos() ),
        )
    }

    pub fn rotation_z(a: f32) -> Mat3 {
        Mat3::new(
            (a.cos(), -a.sin(), 0.0),
            (a.sin(), a.cos() , 0.0),
            (1.0    , 0.0     , 1.0),
        )
    }
}

impl std::ops::Mul<Mat3> for Mat3 {
    type Output = Mat3;
    // certes
    fn mul(self, m: Mat3) -> Mat3{
        Mat3::new(
            (
                self.a.0 * m.a.0 + self.a.1 * m.b.0 + self.a.2 * m.c.0,
                self.a.0 * m.a.1 + self.a.1 * m.b.1 + self.a.2 * m.c.1,
                self.a.0 * m.a.2 + self.a.1 * m.b.2 + self.a.2 * m.c.2,
            ),
            (
                self.b.0 * m.a.0 + self.b.1 * m.b.0 + self.b.2 * m.c.0,
                self.b.0 * m.a.1 + self.b.1 * m.b.1 + self.b.2 * m.c.1,
                self.b.0 * m.a.2 + self.b.1 * m.b.2 + self.b.2 * m.c.2,
            ),
            (
                self.c.0 * m.a.0 + self.c.1 * m.b.0 + self.c.2 * m.c.0,
                self.c.0 * m.a.1 + self.c.1 * m.b.1 + self.c.2 * m.c.1,
                self.c.0 * m.a.2 + self.c.1 * m.b.2 + self.c.2 * m.c.2,
            ),
        )
    }
}

impl std::ops::Mul<Vec3> for Mat3 {
    type Output = Vec3;
    // certes
    fn mul(self, v: Vec3) -> Vec3{
        Vec3::new(
            v.x * self.a.0 + v.y * self.a.1 + v.z * self.a.2,
            v.x * self.b.0 + v.y * self.b.1 + v.z * self.b.2,
            v.x * self.c.0 + v.y * self.c.1 + v.z * self.c.2,
        )
    }
}

/// interpolates between two vectors
/// ratio is where you are btwn the vectors. 0 = 100% p1, 1 = 100%p2
pub fn linear_interpolation_vec3(p1: &Vec3, p2: &Vec3, ratio: f32) -> Vec3 {
    let inv_ratio = 1.0 - ratio;
    Vec3::new(
        p1.x * inv_ratio + p2.x * ratio,
        p1.y * inv_ratio + p2.y * ratio,
        p1.z * inv_ratio + p2.z * ratio,
    )
}

/// Intersection point between a line (p1, p2) and the plane z = d
pub fn line_zplane_intersect(p1: &Vec3, p2: &Vec3, d: f32) -> Vec3 {
    let dir = *p2 - *p1;
    // compute t so that p1.z + t*dir.z = d
    // here, p1 is the "start" of the line, and p2 acts as the direction vector
    let t: f32 = (d - p1.z) / dir.z;
    let y: f32 = p1.y + t * dir.y;
    let x: f32 = p1.x + t * dir.x;
    Vec3::new(x, y, d)
}

#[cfg(test)]
mod utils_test {
    use super::*;

    #[test]
    fn test_zplane_intersect() {
        let p1 = Vec3::new(1.0, 1.0, 0.0);
        let p2 = Vec3::new(1.0, 1.0, 2.0);
        let p3 = Vec3::new(2.0, 2.0, 2.0);
        let p4 = Vec3::new(1.0, 1.0, 1.0);
        let d = 1.0;

        assert_eq!(line_zplane_intersect(&p1, &p2, d), Vec3::new(1.0, 1.0, 1.0));
        assert_eq!(line_zplane_intersect(&p1, &p3, d), Vec3::new(1.5, 1.5, 1.0));
        line_zplane_intersect(&p4, &p4, d);
    }

    #[test]
    fn this_test_has_so_little_coverage() {
        let p1 = Vec3::new(1.0, 1.0, 1.0);
        let p2 = Vec3::new(2.0, 2.0, 2.0);
        let ratio = 0.75;
        assert_eq!(
            Vec3::new(1.75, 1.75, 1.75),
            linear_interpolation_vec3(&p1, &p2, ratio)
        );
    }
}
