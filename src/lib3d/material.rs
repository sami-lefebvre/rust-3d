//! Gives meshes more color & more texture

use std::rc::Rc;

use crate::colors;
use crate::l2d;
use crate::Color;

#[derive(Clone)]
pub struct Material {
	pub color: Color,
	pub texture: Option<Rc<Texture>>,
	pub emissive: bool,
}

pub struct Texture {
	pub image: l2d::Buffer<l2d::Color>,
}

impl Material{
	pub fn with_color(color: Color) -> Material {
		Material{
			color,
			texture: None,
			emissive: false,
		}
	}

	pub fn with_texture(texture: Rc<Texture>) -> Material {
		Material {
			color: colors::WHITE,
			texture: Some(texture),
			emissive: false,
		}
	}
}

impl Default for Material {
	fn default() -> Material {
		Material::with_color(colors::WHITE)
	}
}

impl Texture {
	pub fn new(image: l2d::Buffer<l2d::Color>) -> Texture {
		Texture {
			image,
		}
	}
}
