//! The 2d module which will take care of 2d drawing.
//! It mainly revolves around the [`Buffer<T>`] class.
//!
//! This module can not spawn a window a draw directly on screen. Instead, it can return a pixel buffer where each pixel is represented by u32 value that can be used by a rendering library (minifb for instance).

use core::ops;
use std::{fmt, u8};
use crate::l2d;

/// The main class that holds the pixel data
///
/// # Examples
///
/// Basic usage:
///
/// ```
/// # use lib3d::l2d::*;
/// let mut buffer = Buffer::<u32>::new(400, 300);
///
/// let triangle = Triangle::new(Pixel::new(10, 10), Pixel::new(70, 20), Pixel::new(25, 40));
///
/// buffer.fill(colors::WHITE.value);
/// buffer.draw_solid_triangle(&triangle, colors::RED.value);
///
/// let buffer_to_use_with_rendering_engine = buffer.get_buffer();  
/// ```

#[derive(Clone)]
pub struct Buffer<T> {
    width: u32,
    height: u32,
    buffer: Vec<T>,
}

pub struct Triangle {
    points: [Pixel; 3],
}

pub struct BarycentricCoord {
	pub u: f32,
	pub v: f32,
	pub w: f32,
}

/// A representation of a 2d rasterized triangle that holds some data from the 3d world
/// 
/// This data is stored into each [ProjectedPoint] itself, so that you can
/// manipulate the vertices and retain these informations. (ex.: sort) 
pub struct RasterizedTriangle {
    points: [ProjectedPoint; 3],
}

#[derive(Copy, Clone)]
pub struct Pixel {
    pub x: i32,
    pub y: i32,
}

/// A representation of a rasterized pixel, containing the (x, y) location of the pixel as well
/// as vital information from the 3d geometry (z-value, color, texture coordinates)
#[derive(Copy, Clone)]
pub struct ProjectedPoint {
    pub x: i32,
    pub y: i32,
    pub z: f32, //depth
    pub color: l2d::Color,
    /// texture coordinate
    pub tc: (f32, f32),
}

#[derive(Copy, Clone, Default)]
pub struct Color {
    pub value: u32,
}

// Use type to make more general. color => value (u32) bc that doesnt change a thing + we can use it as a depth buffer
impl<T: Clone + Copy + Default> Buffer<T> {
    pub fn new(_width: u32, _height: u32) -> Buffer<T> {
        let mut _buffer = vec![T::default(); (_width * _height) as usize];

        Buffer {
            width: _width,
            height: _height,
            buffer: _buffer,
        }
    }

    pub fn get_width(&self) -> u32 {
        self.width
    }

    pub fn get_height(&self) -> u32 {
        self.height
    }

    /// We don't talk about draw_big_pixel
    pub fn draw_big_pixel(&mut self, point: Pixel, value: T) {
        //self.draw_pixel(point, color);
        self.draw_pixel(&Pixel::new(point.x - 1, point.y - 1), value);
        self.draw_pixel(&Pixel::new(point.x, point.y - 1), value);
        self.draw_pixel(&Pixel::new(point.x + 1, point.y - 1), value);
        self.draw_pixel(&Pixel::new(point.x - 1, point.y), value);
        self.draw_pixel(&Pixel::new(point.x, point.y), value);
        self.draw_pixel(&Pixel::new(point.x + 1, point.y), value);
        self.draw_pixel(&Pixel::new(point.x - 1, point.y + 1), value);
        self.draw_pixel(&Pixel::new(point.x, point.y + 1), value);
        self.draw_pixel(&Pixel::new(point.x + 1, point.y + 1), value);
    }

    pub fn draw_pixel(&mut self, point: &Pixel, value: T) {


        if self.is_valid(&point) {
            let slice = self.get_index(point.x, point.y);
            self.buffer[slice] = value;
        }
    }

    pub fn draw_pixel_no_check(&mut self, point: &Pixel, value: T) {
            let slice = self.get_index(point.x, point.y);
            self.buffer[slice] = value;
    }
    
    pub fn draw_pixel_raw_coord(&mut self, pos: &usize, value: T) {
    	self.buffer[*pos] = value;
    }
    
    pub fn get_value(&self, point: &Pixel) -> Option<T> {
    	// let x = (self.get_width() / 2) as i32 + point.x;
        // let y = (self.get_height() / 2) as i32 - point.y;
        
        if self.is_valid(&Pixel::new(point.x, point.y)) {
    		Some(self.buffer[self.get_index(point.x,point.y)])
    	} else {
    		None
    	}
    }

    pub fn get_value_no_check(&self, point: &Pixel) -> T{
    	// let x = (self.get_width() / 2) as i32 + point.x;
        // let y = (self.get_height() / 2) as i32 - point.y;

    	self.buffer[self.get_index(point.x, point.y)]

    }

    pub fn is_valid(&self, point: &Pixel) -> bool {
        point.x >= 0 && point.x < self.width as i32 && point.y >= 0 && point.y < self.height as i32
    }

    pub fn draw_line(&mut self, start: Pixel, end: Pixel, value: T) {
        let mut xa = start.x;
        let mut ya = start.y;
        let mut xb = end.x;
        let mut yb = end.y;

        let mut coeff = (yb - ya) as f32 / (xb - xa) as f32;
        let mut y_offset = ya;
        let mut x_offset = xa;

        // si la ligne est sous y = x
        if (-1.0..=1.0).contains(&coeff) {
            if xa > xb {
                std::mem::swap(&mut xa, &mut xb);

                y_offset = yb;
            }
            for x in xa..xb + 1 {
                let y = y_offset + ((x - xa) as f32 * coeff).round() as i32;
                self.draw_pixel(&Pixel::new(x, y), value);
            }
        }
        // pour quand la ligne est au-dessus de y = x
        else {
            coeff = (xb - xa) as f32 / (yb - ya) as f32;
            if ya > yb {
                std::mem::swap(&mut ya, &mut yb);

                x_offset = xb;
            }
            for y in ya..yb + 1 {
                let x = x_offset + ((y - ya) as f32 * coeff).round() as i32;
                self.draw_pixel(&Pixel::new(x, y), value);
            }
        }
    }

    pub fn draw_triangle(&mut self, tri: &Triangle, value: T) {
        let points = tri.get_points();

        let p0 = points[0];
        let p1 = points[1];
        let p2 = points[2];
        
        //if self.is_valid(p0) && self.is_valid(p1) && self.is_valid(p2) {

		    self.draw_line(p0, p1, value);
		    self.draw_line(p0, p2, value);
		    self.draw_line(p1, p2, value);
       // }
    }

    // https://www.gabrielgambetta.com/computer-graphics-from-scratch/filled-triangles.html
    pub fn draw_solid_triangle(&mut self, tri: &Triangle, value: T) {
        // points are already sorted in the triangle (by Y order)

        let points = tri.get_points();

        let p0 = points[0];
        let p1 = points[1];
        let p2 = points[2];

	// i dont remember what this is for but see if we can remove that
        let x02 = interpolate(&p0.y, &p0.x, &p2.y, &p2.x);
        let mut x012 = interpolate(&p0.y, &p0.x, &p1.y, &p1.x);
        x012.remove(x012.len() - 1);

        x012.extend(interpolate(&p1.y, &p1.x, &p2.y, &p2.x));

        let m = (p1.y - p0.y) as usize; // values are sorted on x so is ok ez

        let x_left;
        let x_right;

        if x02[m] < x012[m] {
            x_left = x02;
            x_right = x012;
        } else {
            x_left = x012;
            x_right = x02;
        }

        for y in p0.y..p2.y + 1 {
            // println!("y: {}", y);
            for x in x_left[(y - p0.y) as usize]..x_right[(y - p0.y) as usize] + 1 {
                // println!("  x: {}", x);
                self.draw_pixel(&Pixel::new(x, y), value)
            }
        }
    }

    pub fn fill(&mut self, value: T) {
        for i in self.buffer.iter_mut() {
            *i = value
        }
    }

    pub fn get_buffer(&self) -> &Vec<T> {
        &self.buffer
    }

    pub fn get_buffer_mut(&mut self) -> &mut Vec<T> {
        &mut self.buffer
    }

    fn get_index(&self, x: i32, y: i32) -> usize {
        (y * self.width as i32 + x) as usize
    }

    pub fn display_nbr(&mut self, number:u32, offx: i32, offy: i32, value: T) {
        let xsize = 16;
        let ysize = 32;
        let space = 24;

        let mut number = number;
        let mut offx = offx;

        let mut power_of_10 = 0;

        while number > 10_u32.pow(power_of_10) {
            power_of_10 += 1;
        }


        for i in (0..power_of_10).rev() {

            let digit = number / 10_u32.pow(i);
            number -= digit * 10_u32.pow(i);

            match digit {
                1 => self.draw_line(Pixel::new(offx + xsize/2, offy), Pixel::new(offx + xsize/2, offy + ysize), value),
                2 => self.connected_lines(vec![
                        Pixel::new(offx        , offy),
                        Pixel::new(offx + xsize, offy),
                        Pixel::new(offx + xsize, offy + ysize / 2),
                        Pixel::new(offx        , offy + ysize),
                        Pixel::new(offx + xsize, offy + ysize),
                    ], value),
                3 => self.connected_lines(vec![
                        Pixel::new(offx,         offy),
                        Pixel::new(offx + xsize, offy),
                        Pixel::new(offx,         offy + ysize / 2),
                        Pixel::new(offx + xsize, offy + ysize),
                        Pixel::new(offx, offy + ysize),
                    ], value),
                4 => self.connected_lines(vec![
                        Pixel::new(offx + xsize, offy + ysize),
                        Pixel::new(offx + xsize, offy),
                        Pixel::new(offx        , offy + ysize / 2),
                        Pixel::new(offx + xsize, offy + ysize / 2),
                    ], value),
                5 => self.connected_lines(vec![
                        Pixel::new(offx + xsize, offy),
                        Pixel::new(offx        , offy),
                        Pixel::new(offx        , offy + ysize / 2),
                        Pixel::new(offx + xsize, offy + ysize / 2),
                        Pixel::new(offx        , offy + ysize),
                    ], value),
                6 => self.connected_lines(vec![
                        Pixel::new(offx + xsize, offy),
                        Pixel::new(offx        , offy + ysize / 2),
                        Pixel::new(offx + xsize, offy + ysize / 2),
                        Pixel::new(offx + xsize, offy + ysize),
                        Pixel::new(offx        , offy + ysize),
                        Pixel::new(offx        , offy + ysize / 2),
                    ], value),
                7 => self.connected_lines(vec![
                        Pixel::new(offx        , offy),
                        Pixel::new(offx + xsize, offy),
                        Pixel::new(offx        , offy + ysize),
                    ], value),
                8 => {
                    self.connected_lines(vec![
                        Pixel::new(offx        , offy),
                        Pixel::new(offx + xsize, offy),
                        Pixel::new(offx + xsize, offy + ysize),
                        Pixel::new(offx        , offy + ysize),
                        Pixel::new(offx        , offy),
                    ], value);
                    self.draw_line(Pixel::new(offx, offy + ysize / 2), Pixel::new(offx + xsize, offy + ysize / 2), value)
                }
                9 => self.connected_lines(vec![
                        Pixel::new(offx + xsize, offy + ysize / 2),  
                        Pixel::new(offx        , offy + ysize / 2),     
                        Pixel::new(offx        , offy),
                        Pixel::new(offx + xsize, offy),
                        Pixel::new(offx + xsize, offy + ysize / 2),
                        Pixel::new(offx        , offy + ysize),
                    ], value),
                0 => self.connected_lines(vec![
                        Pixel::new(offx        , offy),
                        Pixel::new(offx + xsize, offy),
                        Pixel::new(offx + xsize, offy + ysize),
                        Pixel::new(offx        , offy + ysize),
                        Pixel::new(offx        , offy),
                    ], value),


                _ => ()
            }

            offx += space;
        }
    }

    fn connected_lines(&mut self, mut points: Vec::<Pixel>, value: T) {
        let mut previous_point = points.pop().unwrap(); //i know
        for i in points.iter().rev() {
            let new_point = i;
            self.draw_line(previous_point, *new_point, value);
            previous_point = *new_point;
        }
    }
}

// there's no way i didnt steal this from somewhere
// i think from a pseudo code tutorial but i forgot which 
// not used for shading now but interpolation
/*
pub fn draw_shaded_triangle(buffer: &mut Buffer<f32>, tri: &ShadedTriangle<f32>) {
    let points = tri.get_points();

    let p0 = points[0];
    let p1 = points[1];
    let p2 = points[2];

    let x02 = interpolate(p0.y, p0.x, p2.y, p2.x);
    let mut x012 = interpolate(p0.y, p0.x, p1.y, p1.x);
    x012.remove(x012.len() - 1);
    x012.extend(interpolate(p1.y, p1.x, p2.y, p2.x));

    // h = shade
    let h02 = interpolate_float(p0.y, p0.h, p2.y, p2.h);
    let mut h012 = interpolate_float(p0.y, p0.h, p1.y, p1.h);
    h012.remove(h012.len() - 1);
    h012.extend(interpolate_float(p1.y, p1.h, p2.y, p2.h));

    let m = (p1.y - p0.y) as usize; // values are sorted on x so is ok ez

    let x_left;
    let x_right;

    let h_left;
    let h_right;

    if x02[m] < x012[m] {
        x_left = x02;
        x_right = x012;
        h_left = h02;
        h_right = h012;
    } else {
        x_left = x012;
        x_right = x02;
        h_left = h012;
        h_right = h02;
    }

    for y in p0.y..p2.y + 1 {
        let x_l = x_left[(y - p0.y) as usize];
        let x_r = x_right[(y - p0.y) as usize];

        let h_l = h_left[(y - p0.y) as usize];
        let h_r = h_right[(y - p0.y) as usize];

        let h_segment = interpolate_float(x_l, h_l, x_r, h_r);
        for x in x_l..x_r + 1 {
            let shaded_color:f32 = h_segment[(x - x_l) as usize];
            buffer.draw_pixel(Point::new(x, y), shaded_color)
        }
    }
}*/


impl Pixel {
    pub fn new(x: i32, y: i32) -> Pixel {
        Pixel {x, y}
    }
    
    pub fn distance(a: Pixel, b: Pixel) -> f32 {
    	(((b.x - a.x).pow(2) + (b.y - a.y).pow(2)) as f32).sqrt()
    }
}

impl fmt::Display for Pixel {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

impl ProjectedPoint {
    pub fn new(x: i32, y: i32, z: f32) -> ProjectedPoint {
        ProjectedPoint {
            x,
            y,
            z,
            color: l2d::colors::BLACK,
            tc: (0.0, 0.0),
        }
    }
    
    pub fn from_point(pos: Pixel, z: f32) -> ProjectedPoint {
    	ProjectedPoint {
            x: pos.x,
            y: pos.y,
            z,
            color: l2d::colors::BLACK,
            tc: (0.0, 0.0),
        }
    }

    pub fn from_point_with_tc(pos: Pixel, z: f32, tc: (f32, f32)) -> ProjectedPoint {
        ProjectedPoint {
            x: pos.x,
            y: pos.y,
            z,
            color: l2d::colors::BLACK,
            tc,
        }
    }
    
    pub fn to_point(&self) -> Pixel {
    	Pixel::new(self.x, self.y)
    }
}

impl Triangle {
    /// The constructor for Triangle
    ///
    /// Intersting to note: the points are sorted by Y values at creation (for the [`interpolate`] method)
    ///
    /// [`interpolate`]: #method.interpolate
    pub fn new(point1: Pixel, point2: Pixel, point3: Pixel) -> Triangle {
        let mut points = [Pixel::new(0, 0); 3];
        points[0] = point1;
        points[1] = point2;
        points[2] = point3;

        Triangle { points }
    }

    /// Useful for triangle drawing
    pub fn sort_y(&mut self) {
        self.points.sort_by_key(|k| k.y);
    }

    pub fn get_points(&self) -> &[Pixel; 3] {
        &self.points
    }
    
    // ok so barycentric coordinates
    // well explained here https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/barycentric-coordinates
    
    // but basically
    // for a point inside a triangle (which is the only case we care about)
    // you take take the three triangles made by drawing lines from the point to the vertices
    // you calculate their areas and normalize them so that u + v + w = 1.
    // now each value expresses basically a % of the total area
    // to find the weigth each vertice has on the point p,
    // you multiply the value that a vertex has and multiply it by the area oposite of that vertex
    // and now you can do a weighed average and you have a way to interpolate vertices values in the triangle
    
    //idk if this is really barycentric cause if the point is outside the triangle, u + v + w > 1
    pub fn get_barycentric_coord(&self, p: &Pixel) -> BarycentricCoord {
        self.get_barycentric_coord_opt(p, &self.get_area())
    }

    // use this if you already have the area
    /*pub fn get_barycentric_coord_opt(&self, p: &Point, area:&f32) -> BarycentricCoord {
    	let u = Triangle::get_area_static(&self.points[1], &self.points[2], p) / area; // relates to vertex a
    	let v = Triangle::get_area_static(&self.points[0], &self.points[2], p) / area;
    	// let w = 1.0 - (u + v);
    	// on fait pas ça au cas où p hors du triangle (cas où triangle est très aigu car comme les coordonnées sont discrètes
        // il existe un cas où aucune valeur discrète est à l'intérieur du triangle.
    	let w = Triangle::get_area_static(&self.points[0], &self.points[1], p) / area;
    	
    	
    	BarycentricCoord {
    		u,
    		v,
    		w,
    	}
    }*/


    
    pub fn get_barycentric_coord_opt(&self, p: &Pixel, area:&f32) -> BarycentricCoord {
        let a = &self.points[0];
        let b = &self.points[1];
        let c = &self.points[2];

        // 0.5 * ((b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y)).abs() as f32

        BarycentricCoord {
    		u: 0.5 * ((b.x - p.x) * (c.y - p.y) - (c.x - p.x) * (b.y - p.y)).abs() as f32 / area,
    		v: 0.5 * ((p.x - a.x) * (c.y - a.y) - (c.x - a.x) * (p.y - a.y)).abs() as f32 / area,
    		w: 0.5 * ((b.x - a.x) * (p.y - a.y) - (p.x - a.x) * (b.y - a.y)).abs() as f32 / area,
    	}
    }
    
    pub fn get_area(&self) -> f32 {
    	let a = self.points[0];
    	let b = self.points[1];
    	let c = self.points[2];
    	
    	let v1 = Pixel::new(b.x - a.x, b.y - a.y);
    	let v2 = Pixel::new(c.x - a.x, c.y - a.y);
    	
    	0.5 * (v1.x * v2.y - v2.x * v1.y).abs() as f32
    }

    pub fn get_area_static(a: &Pixel, b: &Pixel, c: &Pixel) -> f32 {
    	//let v1 = Point::new(b.x - a.x, b.y - a.y);
    	//let v2 = Point::new(c.x - a.x, c.y - a.y);
    	
    	0.5 * ((b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y)).abs() as f32
    }
}

impl RasterizedTriangle {
    pub fn new(point0: ProjectedPoint, point1: ProjectedPoint, point2: ProjectedPoint) -> RasterizedTriangle {
        let mut points = [ProjectedPoint::new(0, 0, f32::default()); 3];
        points[0] = point0;
        points[1] = point1;
        points[2] = point2;

        RasterizedTriangle { points }
    }

    pub fn new_sorted(point0: ProjectedPoint, point1: ProjectedPoint, point2: ProjectedPoint) -> RasterizedTriangle {
        let mut points = [ProjectedPoint::new(0, 0, f32::default()); 3];
        points[0] = point0;
        points[1] = point1;
        points[2] = point2;

        points.sort_by_key(|k| k.y);

        RasterizedTriangle {points}
    }

    pub fn get_points(&self) -> &[ProjectedPoint; 3] {
        &self.points
    }

    pub fn get_points_mut(&mut self) -> &mut [ProjectedPoint; 3] {
        &mut self.points
    }

    pub fn sort_y(&mut self) {
        self.points.sort_by_key(|k| k.y);
    }
    
	pub fn to_triangle(&self) -> Triangle {
		Triangle::new(
			self.points[0].to_point(),
			self.points[1].to_point(),
			self.points[2].to_point(),
		)
	}

    /// Returns a ProjectedPoint interpolated from the triangle's vertices at position `pos`
    /// 
    /// `area` argument to not have to recompute it in the inner loops if we have the info
    pub fn interpolate_vertices_at_pos(&self, pos: &Pixel, area: f32) -> ProjectedPoint {
        let bary = self.get_barycentric_coord_opt(pos, &area);
        let bary_sum = bary.u + bary.v + bary.w;

        let z = (
            self.points[0].z * bary.u +
            self.points[1].z * bary.v +
            self.points[2].z * bary.w
        ) / bary_sum;

        let r = (
            self.points[0].color.r() as f32 * bary.u +
            self.points[1].color.r() as f32 * bary.v +
            self.points[2].color.r() as f32 * bary.w
        ) / bary_sum;

        let g = (
            self.points[0].color.g() as f32 * bary.u +
            self.points[1].color.g() as f32 * bary.v +
            self.points[2].color.g() as f32 * bary.w
        ) / bary_sum;

        let b = (
            self.points[0].color.b() as f32 * bary.u +
            self.points[1].color.b() as f32 * bary.v +
            self.points[2].color.b() as f32 * bary.w
        ) / bary_sum;

        let color = (r  as u32) << 16 | (g  as u32) << 8 | b  as u32;
        let color = Color{value: color};

        let coordx = (
            bary.u * (self.points[0].tc.0) +
            bary.v * (self.points[1].tc.0) +
            bary.w * (self.points[2].tc.0)
        ) / bary_sum;

        let coordy = (
            bary.u * (self.points[0].tc.1) +
            bary.v * (self.points[1].tc.1) +
            bary.w * (self.points[2].tc.1)
        ) / bary_sum;


        ProjectedPoint {
            x: pos.x,
            y: pos.y,
            z,
            color,
            tc: (coordx, coordy),
        }
    }

    pub fn get_barycentric_coord_opt(&self, p: &Pixel, area:&f32) -> BarycentricCoord {
        let a = &self.points[0];
        let b = &self.points[1];
        let c = &self.points[2];

        // 0.5 * ((b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y)).abs() as f32

        BarycentricCoord {
    		u: 0.5 * ((b.x - p.x) * (c.y - p.y) - (c.x - p.x) * (b.y - p.y)).abs() as f32 / area,
    		v: 0.5 * ((p.x - a.x) * (c.y - a.y) - (c.x - a.x) * (p.y - a.y)).abs() as f32 / area,
    		w: 0.5 * ((b.x - a.x) * (p.y - a.y) - (p.x - a.x) * (b.y - a.y)).abs() as f32 / area,
    	}
    }
    
    pub fn get_area(&self) -> f32 {
    	let a = self.points[0];
    	let b = self.points[1];
    	let c = self.points[2];
    	
    	let v1 = Pixel::new(b.x - a.x, b.y - a.y);
    	let v2 = Pixel::new(c.x - a.x, c.y - a.y);
    	
    	0.5 * (v1.x * v2.y - v2.x * v1.y).abs() as f32
    }

}

impl Color {
    // tkt même pas : volé du tuto minifb : https://docs.rs/minifb/0.19.1/minifb/struct.Window.html#method.update_with_buffer
    pub fn from_rgb(r: u8, g: u8, b: u8) -> Color {
        Color {
            value: ((r as u32) << 16) | ((g as u32) << 8) | b as u32,
        }
    }

    pub fn new(value: u32) -> Color {
        Color { value }
    }

    /// ratio of 0 = a, ratio of 1 = b
    pub fn interpolate(a: &Color, b: &Color, ratio: f32) -> Color {
        if ratio > 1.0 || ratio < 0.0 {panic!("no")}
        Color::from_rgb((a.r() as f32 * (1.0 - ratio) + b.r() as f32 * ratio) as u8, (a.g() as f32 * (1.0 - ratio) + b.g() as f32 * ratio) as u8, (a.b() as f32 * (1.0 - ratio) + b.b() as f32 * ratio) as u8)
    }
    
    pub fn r(&self) -> u8 {
    	((self.value & 0xFF0000) >> 16) as u8
    }
    
    pub fn g(&self) -> u8 {
    	((self.value & 0x00FF00) >> 8) as u8
    }
    
    pub fn b(&self) -> u8 {
    	(self.value & 0x0000FF) as u8
    }
}

impl ops::Mul<f32> for Color {
    type Output = Color;

    fn mul(self, f: f32) -> Color {
        let mut array = self.value.to_be_bytes();

        for i in &mut array {
            *i = (*i as f32 * f) as u8;
        }

        Color {
            value: u32::from_be_bytes(array),
        }
    }
}

impl ops::Mul<Color> for Color {
    type Output = Color;

    fn mul(self, c: Color) -> Color {
        let r = (self.r() as u32 * c.r() as u32) / 255;
        let g = (self.g() as u32 * c.g() as u32) / 255;
        let b = (self.b() as u32 * c.b() as u32) / 255;

        Color::from_rgb(r as u8, g as u8, b as u8)
    }
}

#[allow(dead_code)]
pub mod colors {
    pub static BLACK: super::Color = super::Color { value: 0 };
    pub static WHITE: super::Color = super::Color { value: u32::MAX };
    pub static RED: super::Color = super::Color { value: 0xFF0000 };
    pub static GREEN: super::Color = super::Color { value: 0x00FF00 };
    pub static BLUE: super::Color = super::Color { value: 0x0000FF };
    pub static MAGENTA: super::Color = super::Color { value: 0xFF00FF };
    pub static YELLOW: super::Color = super::Color { value: 0xFFFF00 };
    pub static CYAN: super::Color = super::Color { value: 0x00FFFF };
}

// https://www.gabrielgambetta.com/computer-graphics-from-scratch/lines.html
pub fn interpolate(i0: &i32, d0: &i32, i1: &i32, d1: &i32) -> Vec<i32> {
    if i0 == i1 {
        return vec![*d0; 1];
    }

    /*let i0 = i0 as i64;
    let d0 = d0 as i64;
    let i1 = i1 as i64;
    let d1 = d1 as i64;*/

    let mut values = Vec::with_capacity((i1 - i0 + 1) as usize);

    let a = (d1 - d0) as f32 / (i1 - i0) as f32;
    let mut d = *d0 as f32;
    for _i in *i0..*i1 + 1 {
        values.push(d.round() as i32);
        d += a;
    }
    values
}

pub fn interpolate_float(i0: i32, d0: f32, i1: i32, d1: f32) -> Vec<f32> {
    if i0 == i1 {
        return vec![d0; 1];
    }

    //let i0 = i0 as f32;
    //let d0 = d0 as f32;
    //let i1 = i1 as f32;
    //let d1 = d1 as f32;

    let mut values = Vec::new();

    let a = (d1 - d0) / ((i1 - i0) as f32);
    let mut d = d0;
    for _i in i0..i1 + 1 {
        values.push(d);
        d += a;
    }
    values
}
