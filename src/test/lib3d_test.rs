use super::*;

#[test]
fn test_mat3_mult_mat3() {
	let m1 = Mat3::new(
		(1.0, 2.0, 3.0),
		(4.0, 5.0, 6.0),
		(7.0, 8.0, 9.0)
	);
	let m2 =Mat3::new(
		(11.0, 12.0, 13.0),
		(14.0, 15.0, 16.0),
		(17.0, 18.0, 19.0)
	);
	let res = Mat3::new(
		(90.0, 96.0, 102.0),
		(216.0, 231.0, 246.0),
		(342.0, 366.0, 390.0)
	);
	assert_eq!(m1 * m2, res);
	assert_ne!(m2 * m1, res);
	assert_eq!(Mat3::identity() * m2, m2);
	assert_eq!(m2 * Mat3::identity(), m2);
}

#[test]
fn test_mat3_mult_vec3() {
	let m1 = Mat3::new(
		(1.0, 2.0, 3.0),
		(4.0, 5.0, 6.0),
		(7.0, 8.0, 9.0)
	);
	let v1 =Vec3::new(
		11.0,
		12.0,
		13.0
	);
	let res =Vec3::new(
		74.0,
		182.0,
		290.0
	);
	assert_eq!(Mat3::identity() * v1, v1);
	assert_eq!(m1 * v1, res);
	// assert_ne!(m2 * m1, res);
}

