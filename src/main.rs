#![allow(dead_code)]
#![allow(unused_variables)]

extern crate minifb;
use lib3d::{self, colors, io, magiceye, mesh::Material, renderer::Shading, Renderer};
use rand::random;
use std::cell::RefCell;
use lib3d::scene::Scene;
use minifb::{Key, Scale, Window, WindowOptions};
use std::time::Instant;
use lib3d::{Vec3, Mat3, lighting, renderer};

use crate::lib3d::trans::{Transformable, Transformation};

const WIDTH: usize = 1280;
const HEIGHT: usize = 720;

fn main() {
    let wo = WindowOptions { scale: Scale::X1, ..Default::default() };
    let mut window = Window::new("MDR", WIDTH, HEIGHT, wo).unwrap_or_else(|e| {
        panic!("{}", e);
    });

    let cat = io::load_texture("assets/cat_cropped.png").texture.unwrap().image.clone();
    
    let mut count = 0;
    let now = Instant::now();
    let mut nano_draw = 0_u128;
    let mut nano_transform = 0_u128;
    let mut nano_update_window = 0_u128;
    
    // spitfire.backface_culling = false;

    let mut loop_time = Instant::now();
    // let mut yangle = 0.0;
    let mut fps = 0;
    let mut camera_angle_x = 0_f32;
    let mut camera_angle_y = 0_f32;
    let mut frame_duration = 0.16; //just to initialize with non 0

    let mut wireframe = false;
    let mut frustum_culling = false;
    let mut smooth_shading = true;

    let mut magic = false;

    let mut last_mouse_pos: (f32, f32) = (0.0, 0.0);


    let sensitivity = 0.10;

    let speed = 20.0;

    let mut scene_nbr = 1;
    let mut scene = setup_monkey_scene();

    while window.is_open() && !window.is_key_down(Key::Escape) && !window.is_key_down(Key::E) {
    	// événements
        let mut forward_dir = 0.0;
        let mut sideways_dir = 0.0;

        if window.is_key_down(Key::Up) {
            forward_dir = 1.0;
        }
        
        if window.is_key_down(Key::Down) {
            forward_dir = -1.0;
        }

        if window.is_key_down(Key::Right) {
            sideways_dir = - 1.0;
        }
        if window.is_key_down(Key::Left) {
            sideways_dir = 1.0;
        }

        if window.is_key_pressed(Key::Key1, minifb::KeyRepeat::No) {
            scene_nbr = 1;
            scene = setup_monkey_scene();
        }

        if window.is_key_pressed(Key::M, minifb::KeyRepeat::No) {
            magic = !magic;
        }

        if window.is_key_pressed(Key::I, minifb::KeyRepeat::No) {
            scene_nbr = 2;
            scene = setup_island_scene();
        }
        if window.is_key_pressed(Key::C, minifb::KeyRepeat::No) {
            for m in scene.object_3d_vec_mut() {
                frustum_culling = !frustum_culling;
                // m.frustum_culling = frustum_culling;
                // comment ça soûle ce système nul, à repenser !!!!!
                // hm peut être mettre tous les paramètre dans une struct.
                // ou PLUTÔT: RefCell ou quoi comme ça encore une fois on garde la ref de l'obj ou
                // jsp.
            }
        }
        if window.is_key_pressed(Key::L, minifb::KeyRepeat::No) {
            scene_nbr = 3;
            scene = setup_texture_test();
        }
        if window.is_key_pressed(Key::W, minifb::KeyRepeat::No) {
            wireframe = !wireframe;
            scene.renderer.render_mode = match wireframe {
                false => renderer::RenderMode::Solid,
                true => renderer::RenderMode::Wireframe
            };
        }
        if window.is_key_pressed(Key::S, minifb::KeyRepeat::No) {
            smooth_shading = !smooth_shading;
            for m in scene.objects_3d.iter_mut() {
                let shading = m.get_shading_mut().unwrap();
                *shading = match smooth_shading {
                    false => renderer::Shading::Flat,
                    true =>  renderer::Shading::Smooth,
                };
            }
        }

        fn wow_waves_hopefully(v: Vec3, arg: f32) -> Vec3 {
            Vec3::new(v.x, ((v.x + arg).cos() * (v.z + 1.5 + arg).cos())* 0.1 + 0.1, v.z)
        }

        // waves ??????????????????
        let phase = count as f32 * 0.01;
        if scene_nbr == 2 {
            let ocean = scene.get_object_3d_mut(scene.objects_3d.len() - 1);
            //ocean.apply_fn_to_points(wow_waves_hopefully, phase);
            //ocean.bake_vertex_normals();

            // no more waves in the ocean. That's sad but we'll add them back later when we do a proper animation system
        } else if scene_nbr == 1 {
            scene.lighting.lamps[0].set_position(Vec3::new(0.0, 1.5, -0.02 * count as f32))
        }

        let forward_vec = Vec3::new(speed * frame_duration * sideways_dir, 0.0, speed * frame_duration * forward_dir);

        let mut vector = Mat3::rotation_y(camera_angle_y) * Mat3::rotation_x(camera_angle_x) * forward_vec;
        vector.z = -vector.z;

        scene.camera.translate(vector);

        let scroll = window.get_scroll_wheel();
        if let Some(s) = scroll {
            scene.renderer.focal_length += if s.1 > 0.0 {0.1} else {-0.1};
        }

        if scene_nbr == 1 {
            let cube = scene.get_object_3d_mut(scene.objects_3d.len() - 2);
            cube.set_rotation(Mat3::rotation_y(0.01 * count as f32) * Mat3::rotation_x(0.01 * count as f32))
        }


        scene.camera.rotation = Mat3::rotation_x(camera_angle_x) * Mat3::rotation_y(camera_angle_y);


        let start_trans = Instant::now();

        scene.transform();


        nano_transform += start_trans.elapsed().as_micros();
        

        // DRAW
        let start_draw = Instant::now();

        // cloning for debug (get number of triangle drawn. Not much overhead. Don't do this in real apps though)
        // probably remove derive clone from buffer

        let mut image = scene.render().clone();
        let drawn_triangles = scene.get_drawn_triangle_count();

        nano_draw += start_draw.elapsed().as_micros();
       /*if draw_zbuffer {
        	//pb.fill(lib2d::colors::BLACK.value);
        	let mut index = 0 as usize;
        	for f in renderer.get_depth_buffer().get_buffer() {
        		let color = (((f -2.0) * 500.0).sqrt()).floor() as u8;
        		 
        		image.draw_pixel_raw_coord(index, Color::from_rgb(color, color, color).value);
        		
        		index += 1
        	}
        }*/
        frame_duration = loop_time.elapsed().as_secs_f32();
        loop_time = Instant::now();
        if count % 15 == 0 { 
            fps = (1.0 / frame_duration).floor() as u32;
        }
        image.display_nbr(fps, 10, 10, lib3d::l2d::colors::BLACK.value);
        image.display_nbr(scene.drawn_meshes, 10, 48, lib3d::l2d::colors::BLACK.value);
        image.display_nbr(scene.drawn_triangles, 10, 86, lib3d::l2d::colors::BLACK.value);

        /*let buffer = image.get_buffer_mut();
        for i in 0..buffer.len()  {
            let r = ((((buffer[i] & 0xFF0000) >> 16) / 64) * 64) as u8;
            let g = ((((buffer[i] & 0x00FF00) >> 8) / 64) * 64) as u8;
            let b = (((buffer[i] & 0x0000FF) / 64) * 64) as u8;
            buffer[i] = l2d::Color::from_rgb(r, g, b).value;
        }*/

        let start_update = Instant::now();

        let new_mouse_pos = window.get_mouse_pos(minifb::MouseMode::Pass).unwrap();
        if window.get_mouse_down(minifb::MouseButton::Right) {
            camera_angle_y += (new_mouse_pos.0 - last_mouse_pos.0) * frame_duration * sensitivity;
            camera_angle_x += (new_mouse_pos.1 - last_mouse_pos.1) * frame_duration * sensitivity;
        }
        last_mouse_pos = new_mouse_pos;
        

        if magic {
            let buffer = magiceye::render(&cat, scene.renderer.get_depth_buffer(), magiceye::test_mapping);
            window
            //.update_with_buffer(image.get_buffer(), WIDTH, HEIGHT)
            .update_with_buffer(buffer.get_buffer(), WIDTH, HEIGHT)
            .unwrap();
        } else  {
            let buffer = image.get_buffer();
            window
            //.update_with_buffer(image.get_buffer(), WIDTH, HEIGHT)
            .update_with_buffer(buffer, WIDTH, HEIGHT)
            .unwrap();
        }

        
        nano_update_window += start_update.elapsed().as_micros();

        count += 1;
    }

    let ms = now.elapsed().as_millis();
    println!("-----------------------");
    println!("fps moy.      : {:.1}", 1000.0 * count as f32 / (ms as f32));
    println!("ms / f        : {:.2}", ms as f32 / count as f32);
    println!(
        "ms / f (draw) : {:.2}",
        (nano_draw as f32 / 1000.0) / count as f32
    );
    println!(
        "ms / f (updt) : {:.2}",
        (nano_update_window as f32 / 1000.0) / count as f32
    );
    println!(
        "ms / f (trsf) : {:.2}",
        (nano_transform as f32 / 1000.0) / count as f32
    );
}

fn setup_stereogram_scene() -> Scene {
    let renderer = renderer::Renderer::new(WIDTH as u32, HEIGHT as u32);
    let scene = Scene::new(renderer);
    let cube = lib3d::primitives::cube();
    
    let camera = Transformation{
        translation: Vec3::new(0.0, 0.0, 5.0),
        rotation: Mat3::identity(),
    };

    scene
}

fn setup_island_scene() -> Scene {
    let objs = lib3d::io::load_obj("assets/island/island.obj").unwrap();
    let mut island_meshes = Vec::new();

    for (i, m) in objs.iter().enumerate() {
        if i != 2 && i != 3 {

            let mut m = m.clone();
            if i == 1 {
                m.flip_normals();
            }
            if (4..23).contains(&i) {
                m.backface_culling = false;
            }
            
            island_meshes.push(m);
        }
    }

    let renderer_island = renderer::Renderer::new(WIDTH as u32, HEIGHT as u32);
    let mut scene = Scene::new(renderer_island);

    let ambiant_isl = lighting::AmbiantLighting::new(lib3d::Color::new(1.0, 1.0, 1.0), 1.0);
    let sun_isl= lighting::Sun::new(Vec3::new(1.0, 1.0, -1.0), lib3d::Color::new(0.8, 0.8, 0.7), 1.0);
    scene.renderer.dynamic_range.1 = 1.0;

    scene.lighting.ambiant = ambiant_isl;
    scene.lighting.suns.push(sun_isl);

    for m in island_meshes.iter() {
        let object = Box::new(m.to_owned());
        scene.add_object_3d(object);
    }

    let camera = Transformation{
        translation: Vec3::new(0.0, -2.0, 5.0),
        rotation: Mat3::identity(),
    };

    scene.camera = camera;

    scene

}

fn setup_monkey_scene() -> Scene {
    let objs = lib3d::io::load_obj("assets/nycity.obj").unwrap();
    let nyc = objs[0].to_owned();
    let objs = lib3d::io::load_obj("assets/teapot.obj").unwrap();
    let mut teapot = objs[0].to_owned();
    let objs = lib3d::io::load_obj("assets/suzanne.obj").unwrap();
    let suzanne = objs[0].to_owned();
    let renderer = renderer::Renderer::new(WIDTH as u32, HEIGHT as u32);

    let material = io::load_texture("assets/test-texture.png");
    let mut cube = lib3d::primitives::textured_cube(material);
    cube.translate(Vec3::new(5.0, 0.0, 10.0));

    let mut skybox = lib3d::primitives::cube(); //octosphere(4);
    skybox.frustum_culling = false;
    skybox.paint_all(lib3d::colors::CYAN);
    skybox.scale((50.0, 50.0, 50.0));
    skybox.flip_normals();
    // let skybox = RefCell::new(skybox);

    {
        teapot.merge_vertices(0.0001);
        teapot.bake_face_normals();
        teapot.bake_vertex_normals(); //remove this for a funny texture
        teapot.shading = Shading::Smooth;
        teapot.translate(Vec3::new(0.0, 0.0, 10.0));
    }

    // nyc.scale((5.0, 5.0, 5.0));
    let mut scene = Scene::new(renderer);

    let sun1vec  = Vec3::new(2.0, 4.0, -2.0);
    let sun2vec  = Vec3::new(-2.0, 2.0, 0.0);

    let bigsphereposition = Vec3::new(-8.0, 0.0, 20.0);
    
    let ambiant = lighting::AmbiantLighting::new(lib3d::Color::new(0.8, 0.7, 0.8), 0.1); //4
    let sun1 = lighting::Sun::new(sun1vec, lib3d::Color::new(0.8, 0.7, 0.8), 0.3); //7
    let sun2 = lighting::Sun::new(sun2vec, lib3d::Color::new(0.8, 0.7, 0.8), 0.5); //5
    let lamp = lighting::Lamp::new(Vec3::new(-0.0, 1.5, 2.0), lib3d::colors::MAGENTA, 1.0);
    let biglamp = lighting::Lamp::new(bigsphereposition, lib3d::Color::new(1.0, 0.8, 0.6), 20.0);

    let camera = Transformation{
        translation: Vec3::new(0.0, -2.0, 5.0),
        rotation: Mat3::identity(),
    };

    scene.camera = camera;
    
   
    scene.add_object_3d(Box::new(skybox));

    

    let mut fjdskfjdsk = Vec::new();
    
    for j in 0..10 {
        for i in 0..10 {
            let mut monkey = suzanne.clone();
            monkey.set_position(Vec3::new(-16.0 + 4.0 * i as f32, 0.0, -5.0 * j as f32));
            let color = match random::<u8>() % 9 {
                0 => lib3d::Color::new(1.0, 0.9, 0.8),
                1 => lib3d::Color::new(1.0, 0.8, 0.5),
                2 => lib3d::Color::new(1.0, 0.5, 0.1),
                3 => lib3d::Color::new(1.0, 0.3, 0.1),
                4 => lib3d::Color::new(1.0, 0.2, 0.1),

                5 => lib3d::Color::new(0.8, 0.9, 0.8),
                6 => lib3d::Color::new(0.5, 0.8, 0.8),
                7 => lib3d::Color::new(0.5, 0.5, 0.7),
                8 => lib3d::Color::new(0.5, 0.7, 0.3),
                _ => panic!("math has failed me"),
            };
            monkey.paint_all(color);
            fjdskfjdsk.push(monkey.clone());
            //scene.add_mesh(&a);
        }
    }

    for s in 0..10 {
        let mut sphere = lib3d::primitives::octosphere(s);
        sphere.translate(Vec3::new(5.0 + 2.0 * s as f32, 0.0, 5.0));
        // sphere.paint_all(lib3d::colors::WHITE);
        scene.add_object_3d(Box::new(sphere.to_owned()));
    }

    /*for s in 0..10 {
        let mut sphere = lib3d::primitives::boringsphere(s);
        sphere.translate(Vec3::new(5.0 + 2.0 * s as f32, 5.0, 5.0));
        // sphere.paint_all(lib3d::colors::WHITE);
        scene.add_mesh(RefCell::new(sphere.to_owned()));
    } */

    let mut sphere = lib3d::primitives::octosphere(20);
    sphere.translate(bigsphereposition);
    sphere.scale((5.0, 5.0, 5.0));
    sphere.set_material(Material{color: lib3d::colors::WHITE, texture: None, emissive: true});
    scene.add_object_3d(Box::new(sphere.to_owned()));

    for m in fjdskfjdsk.iter() {
        scene.add_object_3d(Box::new(m.to_owned()));
    }

    let mut plane = lib3d::primitives::plane(79);
    plane.scale((80.0, 80.0, 80.0));
    plane.set_position(Vec3::new(0.0, -5.0, 0.00001)); //to prevent crash lol
    scene.add_object_3d(Box::new(plane));

    scene.lighting.ambiant = ambiant;
    scene.lighting.suns.push(sun1);
    //scene.lighting.suns.push(sun2);
    scene.lighting.lamps.push(lamp);
    scene.lighting.lamps.push(biglamp);

    // scene.add_mesh(&nyc);
    scene.add_object_3d(Box::new(cube));
    scene.add_object_3d(Box::new(teapot));

    scene
}

fn setup_texture_test() -> Scene {
    //let mut objs = lib3d::io::load_obj("assets/kokiri_forest/Kokiri Forest/Kokiri Forest.obj").unwrap();
    let mut objs = lib3d::io::load_obj("assets/link/untitled.obj").unwrap();
    // let mut mesh = &mut objs[1];
    let r = Renderer::new(WIDTH as u32, HEIGHT as u32);
    let mut scene = Scene::new(r);

    // objs[0].set_origin_auto();

    for mesh in objs.iter_mut() {
        mesh.set_origin(Vec3::new(0.0, 0.0, 0.0));
        mesh.scale((0.05, 0.05, 0.05));
        

        // mesh.set_position(pos);
        mesh.shading = Shading::Textured;
        scene.add_object_3d(Box::new(mesh.clone()));
    }

    scene.lighting.suns = vec![lighting::Sun::new(Vec3::new(0.0, 1.0, 0.0), colors::WHITE, 0.5)];
    scene.lighting.ambiant = lighting::AmbiantLighting::new(colors::WHITE, 0.5);
    scene
}
